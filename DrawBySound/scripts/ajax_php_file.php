<?php
if(isset($_FILES["file"]["type"]))
{
	$validextensions = array("jpeg", "jpg", "png");
	$temporary = explode(".", $_FILES["file"]["name"]);
	$file_extension = end($temporary);
	header("Content-Type: application/json", true);
	if ((($_FILES["file"]["type"] == "image/png") || 
		($_FILES["file"]["type"] == "image/jpg") || 
		($_FILES["file"]["type"] == "image/jpeg")) && 
		($_FILES["file"]["size"] < 100000)//Approx. 100kb files can be uploaded.
		&& in_array($file_extension, $validextensions)) {
			if ($_FILES["file"]["error"] > 0)
			{
			echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
			}
			else
			{
				$filePath = "../upload/" . $_FILES["file"]["name"];
				if (file_exists($filePath)) {
					$arrResponse = array('message' => $_FILES["file"]["name"] . " <span id='invalid'><b>already exists.</b></span> ");
				}
				else
				{
					$sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
					$targetPath = $filePath; // Target path where file is to be stored
					move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
					
					$iptc = get_jpg_iptc_metadata($filePath);
					$rasterizeCaption = explode(',', $iptc['RasterizedCaption'][0]);			
					
					$arrResponse = array('message' => "<span id='success'>Image Uploaded Successfully!!</span><br/>", 
										 'linkToImage' => "<a href=". $filePath. ">Here is the link</a>",
										 'metadata' => $rasterizeCaption);				
				}
			}
	}
	else{
		$arrResponse = array('message' => "<span id='invalid'>***Invalid file Size or Type***<span>");
	}

	echo json_encode($arrResponse);
}

function get_jpg_iptc_metadata($path) {
    $size = getimagesize($path, $info);
    if(isset($info['APP13']))
    {    	
        return human_readable_iptc(iptcparse($info['APP13']));
    }
    else {
    	return null;
    }
}

function human_readable_iptc($iptc) {
# From the exiv2 sources
static $iptc_codes_to_names =
array("2#125" => 'RasterizedCaption');
   $human_readable = array();
   foreach ($iptc as $code => $field_value) {
       $human_readable[$iptc_codes_to_names[$code]] = $field_value;
   }
   return $human_readable;
}
?>