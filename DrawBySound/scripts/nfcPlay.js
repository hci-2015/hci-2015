$(document).ready(function(){
	var imageId = getQueryVariable("image");
	console.log(imageId);
	if(imageId != null){
		$.ajax({
		url: "scripts/get_metadata.php", // Url to which the request is send
		type: "GET",             // Type of request to be send, called as method
		data: "imageId=" + imageId, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		//contentType: "application",       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		//processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data)   // A function to be called if request succeeds
		{
			console.log(data);
			if(data.metadata != null && data.linkToImage != null){
				document.getElementById('image').src = data.linkToImage;
				var nfcImageFrequencies = data.metadata;
				createSounds(nfcImageFrequencies);
			}
			else{
				$("#message").html(data.message);
			}
		},
		error: function(request, status, error){
			$('#message').html(status);
		}
	});
	} 
});

function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}