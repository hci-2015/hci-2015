/*partially used code from https://github.com/cwilso/PitchDetect/blob/master/js/pitchdetect.js, adapted and modified*/
window.AudioContext = window.AudioContext || window.webkitAudioContext;

var audioContext = null;
var isPlaying = false;
var analyser = null;
var theBuffer = null;
var mediaStreamSource = null;
var context2D = null;
var canvasX;
var canvasY;
var shapeSize;
var numberOfLines;

var CANVAS_ABSOLUTE_SIZE = 480;
var WHITE_COLOR = "white";
var BLACK_COLOR = "black";

window.onload = function() {
	audioContext = new AudioContext();
	MAX_SIZE = Math.max(4,Math.floor(audioContext.sampleRate/5000));	// corresponds to a 5kHz signal	
	colors = new Colors();	
}

function error() {
    alert('Stream generation failed.');
}

function getUserMedia(dictionary, callback) {
    try {
        navigator.getUserMedia = 
        	navigator.getUserMedia ||
        	navigator.webkitGetUserMedia ||
        	navigator.mozGetUserMedia;
        navigator.getUserMedia(dictionary, callback, error);
    } catch (e) {
        alert('getUserMedia threw exception :' + e);
    }
}

function gotStream(stream) {
    // Create an AudioNode from the stream.
    mediaStreamSource = audioContext.createMediaStreamSource(stream);

    // Connect it to the destination.
    analyser = audioContext.createAnalyser();
    analyser.fftSize = 2048;
    mediaStreamSource.connect( analyser );
    updatePitch();
}

function toggleLiveInput() {
	updateUI();
    if (isPlaying) {
        analyser = null;
        isPlaying = false;
		if (!window.cancelAnimationFrame)
			window.cancelAnimationFrame = window.webkitCancelAnimationFrame;
        window.cancelAnimationFrame( rafID );
    }
    canvas = document.getElementById('canvas');
    context2D = canvas.getContext('2d');
    numberOfLines = 0;
	canvasX = 0;
	canvasY = 0;
	shapeSize = 0;
    for (var i = 0; i < sizes.length; i++) {
    	var settingSizeElement = document.getElementById("settings-size-" + sizes[i]);
    	settingSizeElement.classList.add("disabled");
    	var currentSettingSizeIsSelected = settingSizeElement.classList.contains("active");
        if(currentSettingSizeIsSelected){
        	numberOfLines = sizes[i];
        	shapeSize = CANVAS_ABSOLUTE_SIZE/numberOfLines;        	
        }        
    }   
    getUserMedia(
    	{
            "audio": {
                "mandatory": {
                    "googEchoCancellation": "false",
                    "googAutoGainControl": "false",
                    "googNoiseSuppression": "false",
                    "googHighpassFilter": "false"
                },
                "optional": []
            },
        }, gotStream);
}

var rafID = null;
var tracks = null;
var buflen = 1024;
var buf = new Float32Array( buflen );

var noteStrings = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];

function noteFromPitch( frequency ) {
	var noteNum = 12 * (Math.log( frequency / 440 )/Math.log(2) );
	return Math.round( noteNum ) + 69;
}

function frequencyFromNoteNumber( note ) {
	var frequency = 440 * Math.pow(2,(note-69)/12);
	return frequency;
}

function centsOffFromPitch( frequency, note ) {
	return Math.floor( 1200 * Math.log( frequency / frequencyFromNoteNumber( note ))/Math.log(2) );
}

var MIN_SAMPLES = 0;  // will be initialized when AudioContext is created.

function autoCorrelate( buf, sampleRate ) {
	var SIZE = buf.length;
	var MAX_SAMPLES = Math.floor(SIZE/2);
	var best_offset = -1;
	var best_correlation = 0;
	var rms = 0;
	var foundGoodCorrelation = false;
	var correlations = new Array(MAX_SAMPLES);

	for (var i=0;i<SIZE;i++) {
		var val = buf[i];
		rms += val*val;
	}
	rms = Math.sqrt(rms/SIZE);
	if (rms<0.01) // not enough signal
		return -1;

	var lastCorrelation=1;
	for (var offset = MIN_SAMPLES; offset < MAX_SAMPLES; offset++) {
		var correlation = 0;

		for (var i=0; i<MAX_SAMPLES; i++) {
			correlation += Math.abs((buf[i])-(buf[i+offset]));
		}
		correlation = 1 - (correlation/MAX_SAMPLES);
		correlations[offset] = correlation; // store it, for the tweaking we need to do below.
		if ((correlation>0.9) && (correlation > lastCorrelation)) {
			foundGoodCorrelation = true;
			if (correlation > best_correlation) {
				best_correlation = correlation;
				best_offset = offset;
			}
		} else if (foundGoodCorrelation) {
			var shift = (correlations[best_offset+1] - correlations[best_offset-1])/correlations[best_offset];  
			return sampleRate/(best_offset+(8*shift));
		}
		lastCorrelation = correlation;
	}
	if (best_correlation > 0.01) {		
		return sampleRate/best_offset;
	}
	return -1;
}

function getFrequencyInterval(pitch){
	var frequencyIntervals = colors.FrequencyIntervals;
	if(pitch < frequencyIntervals[0][0] ){
		return -1;
	}
	if(pitch > frequencyIntervals[frequencyIntervals.length - 1][1]){
		return -2;
	}
	for(var i = 0; i<frequencyIntervals.length; i++){
		if(frequencyIntervals[i][0] <= pitch && frequencyIntervals[i][1] > pitch){
			return i;
		}
	}
}

function getShapeColor(color, frequencyInterval){
	for (var i = 0; i<colors.ColorsWithIntensities.length; i++){
			if(color === colors.ColorsWithIntensities[i][0]){
				var colorsI = colors.ColorsWithIntensities[i][1];	
				return colorsI[frequencyInterval];			
			}
		}
}

var allFrequencies = [];
function drawShapeOnCanvas(shapeColor){
	context2D.fillStyle = shapeColor;
		context2D.fillRect(canvasX, canvasY, shapeSize, shapeSize);
		if(canvasX + shapeSize >= CANVAS_ABSOLUTE_SIZE){
			canvasX = 0;
			canvasY += shapeSize;
		}else{
			canvasX += shapeSize;
		}
		var isCanvasFinished = canvasY >= CANVAS_ABSOLUTE_SIZE;
		if(isCanvasFinished){
	        analyser = null;
	        isPlaying = false;
	        showAfterPaintControls();

log("All frequencies: " + allFrequencies);
    	}
}

function tryToUpdatePitch(){
	if(analyser !== null){
		updatePitch();
	}
}

function updatePitch(time) {
    var cycles = new Array;
    analyser.getFloatTimeDomainData(buf);
    var ac = autoCorrelate(buf, audioContext.sampleRate);;
    if (ac == -1) {
        //drawShapeOnCanvas(DEFAULT_WHITE_COLOR);
    } else {
        pitch = ac;
        var note = noteFromPitch(pitch);
        color = colors.colors[note % 12];
        var frequencyInterval = getFrequencyInterval(pitch);
	    allFrequencies.push("" + pitch.toFixed(2) + ""); // will be set as metadata when saving the image	   
        if (frequencyInterval == -1) {
            drawShapeOnCanvas(WHITE_COLOR);
        }
        if (frequencyInterval == -2) {
            drawShapeOnCanvas(BLACK_COLOR);
        } else {
            var colorWithIntensities = colors.ColorsWithIntensities[color];
            var shapeColor = getShapeColor(color, frequencyInterval);
            drawShapeOnCanvas(shapeColor);
        }

    }
    var timeout = "0";
    if (isDelayed == true) {
        timeout = "200";
    }

    setTimeout(function() {
        if (!window.requestAnimationFrame)
            window.requestAnimationFrame = window.webkitRequestAnimationFrame;

        rafID = window.requestAnimationFrame(tryToUpdatePitch);
    }, timeout);
}
