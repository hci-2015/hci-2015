"use strict";

var sizes = ["10", "20", "30", "40", "50"];

var currentSize = 30;
var isDelayed = false;

var saveCanvasSizeSetting = function(event) {
    var targetElement = document.getElementById(event.currentTarget.id);

    if (targetElement.classList.contains("disabled")) {
        return;
    }

    log("Clicked " + event.currentTarget.id);
    for (var i = 0; i < sizes.length; i++) {
        document.getElementById("settings-size-" + sizes[i]).classList.remove("active");
    }
    targetElement.classList.add("active");
    currentSize = event.currentTarget.id.substr(event.currentTarget.id.length - 2);
    log("Current size is now " + currentSize);
    updateCanvasGrid(currentSize);
};

function printObject(object) {
    var output = "";
    for (var property in object) {
        output += property + ": " + object[property] + "\n";
    }
    log(output);
}

function addEvent(element, evnt, funct) {
    if (element.attachEvent) {
        return element.attachEvent("on" + evnt, funct);
    } else {
        return element.addEventListener(evnt, funct, false);
    }
}

function log(text) {
    window.console.log(text);
}

function saveDelayed() {
    var isChecked = document.getElementById("delayed-checkbox").checked;
    log("Delayed ? " + isChecked);
    if (isChecked) {
        isDelayed = true;
    } else {
        isDelayed = false;
    }
}
document.addEventListener("DOMContentLoaded", function() {
    for (var i = 0; i < sizes.length; i++) {
        addEvent(document.getElementById("settings-size-" + sizes[i]), "click", saveCanvasSizeSetting);
    }
    addEvent(document.getElementById("delayed-checkbox"), "click", saveDelayed);
    updateCanvasGrid(currentSize);
    log("All functions are loaded.");
}, false);
