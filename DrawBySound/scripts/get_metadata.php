<?PHP
	$imageId = $_GET[ 'imageId' ];
	header("Content-Type: application/json", true);
	$filePath = "../upload/" . $imageId . ".jpg";
	$imgSource = "upload/" . $imageId . ".jpg";
				if (file_exists($filePath)) {
					$iptc = get_jpg_iptc_metadata($filePath);
					$rasterizeCaption = explode(',', $iptc['RasterizedCaption'][0]);
					$arrResponse = array('metadata' => $rasterizeCaption,
										 'linkToImage' => $imgSource);	
				}else{
					$arrResponse = array('message' => " <span id='invalid'><b>Sorry, file was removed.</b></span> ");
				}
				echo json_encode($arrResponse);

	function get_jpg_iptc_metadata($path) {
    	$size = getimagesize($path, $info);
	    if(isset($info['APP13']))
	    {    	
	        return human_readable_iptc(iptcparse($info['APP13']));
	    }
	    else {
	    	return null;
	    }
	}

	function human_readable_iptc($iptc) {
		# From the exiv2 sources
		static $iptc_codes_to_names =
		array("2#125" => 'RasterizedCaption');
		   $human_readable = array();
		   foreach ($iptc as $code => $field_value) {
		       $human_readable[$iptc_codes_to_names[$code]] = $field_value;
		   }
		   return $human_readable;
	}
?>