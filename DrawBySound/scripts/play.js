var tryToCreateSounds = function(){
	if(uploadedImageFrequencies.length > 0){
		console.log("Started to sing...");
		document.getElementById("play-current-image-button").classList.add("hide");
		createSounds(uploadedImageFrequencies);
		setTimeout(function(){
        	document.getElementById("play-current-image-button").classList.remove("hide");
    	}, uploadedImageFrequencies.length * 200);
	}
}

var createSounds = function(arrayOfFrequencies){
	var audioContext = new AudioContext();
	var osc;
	for(var i = 0; i < arrayOfFrequencies.length; i++)	{
		osc = audioContext.createOscillator();

		osc.frequency.value = arrayOfFrequencies[i];
		osc.connect(audioContext.destination);
		osc.start(i*0.2);
		osc.stop((i+1)*0.2);
	}
}
