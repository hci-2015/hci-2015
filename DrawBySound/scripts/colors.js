var Colors = function(){

	this.colors = ["violet", //violet
	"blue-violet", //blue-violet
	"blue", //blue
	"blue-green", //blue-green
	"green", //green
	"yellow-green", //yellow-green
	"yellow", //yellow
	"yellow-orange", //yellow-orange
	"orange", //orange
	"orange-red", //orange-red
	"red", //red
	"red-violet"];//red-violet

	this.FrequencyIntervals = [[16, 150],
							  [150, 400],
							  [400, 650],
							  [650, 1000],
							  [1000, 1400],
							  [1400, 1800],
							  [1800, 2300],
							  [2300, 4500]];

	this.ColorsWithIntensities = [
		["violet", ["#efebf2", "#e0d7e5", "#c2afcb", "#9574a5", "#68397f", "#482758", "#341c3f", "#291632"]],
		["blue-violet", ["#d5d4e3", "#abaac7", "#9695ba", "#575690", "#2E2C75", "#24235d", "#17163a", "#12112e"]],
		["blue", ["#eae5f9", "#d6ccf4", "#ad99ea", "#5c33d6", "#3401CC", "#24008e", "#1a0066", "#140051"]],
		["blue-green", ["#cceae8", "#99d6d2", "#66c2bc", "#32aea6", "#009A90", "#006b64", "#004d48", "#003d39"]],
		["green", [ "#c4e8c9","", "#8ad293", "#63c36e", "#3DB54B", "#30903c",  "#2a7e34","#1e5a25",  "#18481e"]],
		["yellow-green", ["#f0f6d6", "#e2edad", "#d4e584", "#c6dc5a", "#B8D432", "#809423", "#5c6a19", "#495414"]],
		["yellow", ["#fefccc", "#fef999", "#fef766", "#FEF200", "#e4d900", "#b1a900", "#7f7900", "#323000"]],
		["yellow-orange", ["#fff4cc", "#ffea99", "#ffdb4c", "#FFCC00", "#cca300", "#997a00", "#665100", "#332800"]],
		["orange", ["#ffeacc", "#ffd699", "#ffb74c", "#FF9900", "#cc7a00", "#995b00", "#663d00", "#331e00"]],
		["orange-red", ["#fdeee8", "#f8bea6", "#f27d4e", "#EF5D22", "#bf4a1b", "#8f3714", "#5f250d", "#2f1206"]],
		["red", ["#fee5e8", "#fcb2bc", "#fa4d62", "#F80120", "#c60019","#940013", "#63000c", "#310006"]],
		["red-violet", ["#f2cce2", "#e699c6", "#da66aa", "#ce328e", "#C20072", "#9b005b", "#610039", "#3a0022"]],
	];
}

