"use strict";

var updateUI = function() {
    document.getElementById("start-button").classList.add("disabled");
    document.getElementById("delayed-checkbox").classList.add("disabled");
};

document.addEventListener("DOMContentLoaded", function() {
    addEvent(document.getElementById("start-button"), "click", toggleLiveInput);
    log("All functions from start.js are loaded.");
}, false);
