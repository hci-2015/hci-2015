"use strict";

var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");

var CANVAS_ABSOLUTE_SIZE = 480;

function Shape(coordX, coordY, width, height, fill) {
    this.x = coordX;
    this.y = coordY;
    this.w = width;
    this.h = height;
    this.fill = fill;
}

var updateCanvasGrid = function(canvasSize) {
    log("\n\nUpdating the canvas grid for the value " + canvasSize);

    context.clearRect(0, 0, CANVAS_ABSOLUTE_SIZE, CANVAS_ABSOLUTE_SIZE);

    for (var i = 1; i <= canvasSize; i++) {
        drawShape(newGridLine(CANVAS_ABSOLUTE_SIZE / canvasSize * i, 0));
        drawShape(newGridLine(0, CANVAS_ABSOLUTE_SIZE / canvasSize * i));
    }
};

function newGridLine(coordX, coordY) {
    var width, height;
    if (coordX === 0) {
        width = CANVAS_ABSOLUTE_SIZE;
        height = 1;
    }
    if (coordY === 0) {
        width = 1;
        height = CANVAS_ABSOLUTE_SIZE;
    }
    return new Shape(coordX, coordY, width, height, "#AAA");
}

function drawShape(shape) {
    context.fillStyle = shape.fill;
    context.fillRect(shape.x, shape.y, shape.w, shape.h);
}
