"use strict";


var showAfterPaintControls = function() {
    log("Done painting!");
    // Hide start button.
    document.getElementById("start-button").classList.add("hide");

    // Show save buttons.
    document.getElementById("save-image-button").classList.remove("hide");
    document.getElementById("get-hardcopy-button").classList.remove("hide");
    document.getElementById("reload-button").classList.remove("hide");

    // Show Play button.
    document.getElementById("play-current-image-button").classList.remove("hide");

};

function download() {
    log("Saving the image locally: " + allFrequencies);

    // Save image on server.
    var dataURL = document.getElementById("canvas").toDataURL("image/jpeg");
    log("Data url for canvas: " + dataURL);

    $.ajax({
        type: "POST",
        url: "scripts/writeImage.php",
        data: {
            imgBase64: dataURL
        }
    }).done(function(file) {
        console.log("Image was saved on server: " + file);
        addMetadata(file);
        var fileUrl = "http://students.info.uaic.ro/~alexandrina.filimonov/hci/DrawBySound/scripts/" + file;
        log("Saving locally: " + fileUrl);
        window.open(fileUrl);
    });
}

function addMetadata(filename) {
    // Add metadata for the image.
    $.ajax({
        type: "POST",
        url: "scripts/addMetadata.php",
        data: {
            filepath: filename,
            metadata: allFrequencies.join()
        }
    }).done(function() {
        console.log("Metadata was added on file " + filename);
    });
}

function getUserInformation() {
    log("Showing input for user information.");
    document.getElementById("container-user-information").classList.remove("hide");
}

function thankYou() {
    log("Showing thank you.");
    document.getElementById("submit-button").classList.add("hide");
    document.getElementById("input-group").classList.add("hide");
    document.getElementById("thank-you-note").classList.remove("hide");
}

function playImage(){
    document.getElementById("play-current-image-button").classList.add("hide");
    createSounds(allFrequencies);
    setTimeout(function(){
        document.getElementById("play-current-image-button").classList.remove("hide");
    }, allFrequencies.length * 200);
}

document.addEventListener("DOMContentLoaded", function() {
    addEvent(document.getElementById("save-image-button"), "click", download);
    addEvent(document.getElementById("get-hardcopy-button"), "click", getUserInformation);
    addEvent(document.getElementById("play-current-image-button"), "click", playImage);
    //addEvent(document.getElementById("submit-button"), "click", thankYou);
    log("All functions from afterPainting.js are loaded.");
}, false);
